package com.chenpob.app1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.AlarmClock.EXTRA_MESSAGE
import android.util.Log
import android.widget.TextView
import com.chenpob.app1.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val stuName: TextView = findViewById(R.id.nameText)
        val stuCode: TextView = findViewById(R.id.codeText)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.helloButton.setOnClickListener {
            Log.d(TAG,""+stuName.text)
            Log.d(TAG,""+stuCode.text)

            val message = stuName.text
            val intent = Intent(this,HelloActivity::class.java).apply {
                putExtra(EXTRA_MESSAGE, message)
            }

            startActivity(intent)
        }

    }

    companion object {
        private const val TAG = "MainActivity"
    }
}